import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';

export default (function () {
  $('#dataTable').DataTable();
}());
