@extends('admin.layouts.default')
@section('page-title', 'Users')
@section('content')
    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <a href="{{ route('admin.users.create') }}" class="btn btn-info">Add User</a>
    </div>

    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead class="thead-dark">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Registration Date</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tfoot class="thead-dark">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Registration Date</th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                            {{ $user->name }}
                        </td>
                        <td>
                            {{ $user->email }}
                        </td>
                        <td>
                            {{ \Carbon\Carbon::parse($user->created_at)->toDayDateTimeString() }}
                        </td>
                        <td>
                            <a href="{{ route('admin.users.edit', $user->slug) }}" class="btn btn-primary btn-sm btn-block">Edit</a>
                        </td>
                        <td>
                            {!! Form::model($user, ['route' => ['admin.users.destroy', $user->uuid_text], 'method' => 'DELETE']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm btn-block']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
